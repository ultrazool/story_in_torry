# What's the Story in Torry?

This is a collection of notes, links and tools to help investigate the buildup of the proposed Energy Transition Zone in Torry, Aberdeen.

I first heard about the ETZ from a Torry-based speaker at a climate event. The community there faces the loss of part of their community park, St Fittick's, to build manufacturing facilities for the South Harbour of the Port of Aberdeen.

The development in the park is part of a larger "Energy Transition Zone", supported by Scottish Government through a "[Just Transition](https://www.gov.scot/publications/just-transition-fund/pages/year-one-projects/)" fund.

Why is climate funding being used to enable oil and gas partnerships to build manufacturing facilities on community greenspace? Why in St Fittick's Park, when the nearby municipal golf course remains untouched? 

[Press release from ETZ Ltd on Just Transition funding](https://etzltd.com/news/etz-ltd-welcomes-just-transition-funding-for-key-energy-transition-activities)

[Masterplan Document](https://consultation.aberdeencity.gov.uk/planning/draft-energy-transition-zone-masterplan-consultati/supporting_documents/ETZ_Draft_Masterplan_April_2023_V8_update_clean.pdf)

[Friends of St Fittick's Park](https://saintfittickstorry.com/) - Torry's campaign to save the use of its park.

## Development notes

The ideal here is to use datasette to curate a small collection of related data, and try sharing it with collaborators through [datasette.cloud](https://www.datasette.cloud/). To start with:

* Local area maps extracted from OpenStreetmap using Overpass Turbo
* Shapes for the "Opportunity Zones" and planned new developments, traced if necessary from the Masterplan
* Anything goes! This could include metadata from related planning permission, Companies House records of related entities, land use and habitat classifications. Got to start somewhere. 

### QGIS georeferencing and tracing

I screengrabbed many of the land use plans from the Masterplan Document and then georeferenced them in QGIS and traced off the features by hand. Thankfully the area is small. Workflow:

* Load an XYZ Tiles OpenStreetmap basemap into QGIS, zoom to area of interest
* `Raster... Georeferencer` menu to load a saved screenshot and pick Ground Control Points (minimum three, in different corner of the image, the more the better. I used the corners of significant buildings)
* `Layers... Create Layer` to add a GeoPackage in WGS84 (the GeoJSON default) with a table of Polygon type
* Add descriptive fields, then start tracing over building and land use features.
* Remember to toggle off editing and save first, to generate identifiers and save the layer
* Add the resulting geopackage to the repo

Then do the same thing over again for a new layer with the boundaries of the "Opportunity Zones" of which St Fittick's Park is one

### Export for Datasette

I want to be able to dump this GeoPackage into a form of table per layer with geometry as either single features in GeoJSON, or Well Known Text. The former feels more web-appropriate.

https://datasette.io/for/geospatial - this overview predates the support for sqlite-tg which is new and experimental! This round-trip seems a little silly but let's start with it. You have to specify each layer name as a distinct GeoJSON file. In retrospect I could have avoided ALL this with export to Spatialite from QGIS but one lives and learns

```
ogr2ogr -f GeoJSON -lco COORDINATE_PRECISION=6 zones.json torry.gpkg opportunity_zones
ogr2ogr -f GeoJSON -lco COORDINATE_PRECISION=6 developments.json torry.gpkg new_developments
```

Now we can start using some of the `datasette` utils - [geojson-to-sqlite](https://datasette.io/tools/geojson-to-sqlite)

```
pip install geojson-to-sqlite
geojson-to-sqlite torry.db new_developments developments.json
geojson-to-sqlite torry.db opportunity_zones zones.geojson
```

datasette.cloud will import from an open data portal based on Socrata or from a CSV at a URL or uploaded via the web, API based upload is still incoming

Nip into the `sqlite` shell briefly to remember what schema I created - the generated feature IDs have got lost in the export, and might have been useful.

```sqlite> .schema new_developments
```

```
sqlite3 -header -csv torry.db 'SELECT type, description, geometry FROM new_developments;' > developments.csv
sqlite3 -header -csv torry.db 'SELECT zone_id, description, geometry FROM opportunity_zones;' > zones.csv
```
They look like reasonable data with single GeoJSON features as strings in a geometry field, let's try committing them and adding to datasette.cloud from this gitlab repo

This looks and feels very nice. I'm not sure if I broke something but the service stopped responding shortly afterwards!

### Data from OpenStreetmap

While waiting for other things we can think about a minimum effort way to get relevant GeoJSON out through Overpass Turbo into the same collection (with the `overpass` package from pypi, a query for all ways, and a bounding polygon that we draw in QGIS)

`query.txt` has a minimal Overpass query that will just get everything within a bounding box, which we can run against the Overpass API with `wget` like this
```
 wget --verbose -O torry.osm --post-file=query.txt "http://overpass-api.de/api/interpreter"
```

[OSMtoGeoJSON](https://github.com/tyrasd/osmtogeojson) this is npm, which is in apt so `sudo apt install npm` then `sudo npm install -g npm` and ended up with a version of npm that needed a newer version of node, and now I'm at "I can't be doing with this ecology".

```
npm install -g osmtogeojson
```

Well this is as far as I got the day before setting off to SODU, where I will socialise and get some feedback and come back and improve things

### Datasette-sqlite locally 

The laptop I'm writing this on was recently wiped and doesn't have much of a development environment. It's also 32 bit which is a complication! (It's the same age as my kid, who is sitting Prelims now...)

[Install miniconda - Linux](https://docs.conda.io/projects/miniconda/en/latest/index.html) - the 32 bit installer was last built in 2019, after a bit of failing to find "conda init" or update it, I dropped back to old-style `virtualenv` with the default `python3` on my machine

```python3 -m venv torry
. ./torry/bin/activate
```

I don't know how well this bodes for the rest of the session! The next step is to install datasette locally, get a feel for it, collect data then shift to datasette.cloud

`pip install datasette`

It's [this post about geospatial queries in SQLite with TG](https://til.simonwillison.net/sqlite/sqlite-tg) that prompted me to try this path, so let's follow it:

`datasette install datasette-sqlite-tg`

This fails for me because `pip` on my setup can't find `sqlite-tg`. Head to [sqlite-tg on Github](https://github.com/asg017/sqlite-tg/) and start reading the makefile. Manage to `make loadable` then read the `setup.py` in the python bindings and realise it's this:
```
elif system == "Linux":
    if machine not in ["x86_64"]:
        raise Exception("unsupported platform")
```

Well I suppose this is what the new sqlite-tg support in datasette.cloud is for, next burst I'll focus on data and try it directly.





