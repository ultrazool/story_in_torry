Property holding company, Innovation campus 
https://find-and-update.company-information.service.gov.uk/company/10588799/filing-history
"not a going concern"

https://find-and-update.company-information.service.gov.uk/company/09610476/filing-history
- intermediate holding company

https://find-and-update.company-information.service.gov.uk/company/09064598/filing-history - ultimate holding company

https://en.m.wikipedia.org/wiki/Trust_port - Port of Aberdeen governance mode

https://www.bpaberdeenhydrogenhub.com/about-us/ - bp Aberdeen Hydrogen Hub are a key tenant

Masterplan document - community engagement summary

[200~1 1 1
1
1 2ETZ | Masterplan
Through the engagement outlined above and in dialogue with the local community
a wide range of issues and perspec�ves were raised, with a par�cularly strong
interest in the allocated Opportunity Sites at St Fi�ck’s Park and Bay of Nigg (OP56
and OP62), and the poten�al for resultant impacts on access to greenspace,
biodiversity, and local environmental quality. The key issues and themes of
feedback that emerged across the community consulta�on are summarised below:
Land Use & Economy
• Recogni�on and support for the principle of energy transi�on in Aberdeen,
reducing reliance on oil & gas and transferring skills to the green economy.
• Brownfield land should be priori�sed for development over greenfield sites,
making the most of exis�ng land within Altens and East Tullos.
• �nterest in what type of energy transi�on users, sectors and ac�vi�es would
locate on iden�fied Opportunity Sites, and if there was demand or need for
these in Aberdeen.
• Doonies Farm has been on the current site for many years, and some felt it
should be protected by the City Council rather than allocated for energy
transi�on use.
• Clarity sought on how economic development within ETZ would deliver
opportuni�es for local people in terms of jobs, skills, training.
Parks & Greenspace
• There was significant concern around the loss of a por�on of St Fi�ck’s Park,
which is highly valued by the community in Torry as its main green and open
space.
• Uncertainty as to how much of the park may be temporarily used and restored,
or developed, either by ETZ or by Port of Aberdeen as part of their construc�on
of the South Harbour.
• Concern around the poten�al impacts on local health & well-being (including
mental health) as a result of the loss of greenspace.
Local Environment
• East Tullos Burn was significantly enhanced in 2014 through a SEPA / ACC /
Community partnership project, crea�ng new wetlands which add to the
quality of the park, as well as providing local biodiversity and drainage benefits.
Strong views the Burn should be retained as a key local asset.
• Development at St Fi�ck’s Park is close to residen�al proper�es within Torry,
and there was concern around poten�al for impacts on local amenity, including
from noisy port-related ac�vi�es.
• Poten�al impacts from construc�on must be carefully managed given the
sensi�vity of the local environment and proximity to communi�es.
• There has been previous development in the area, including Ness Energy-from-
Waste Facility (East Tullos) and Aberdeen South Harbour (Bay of Nigg) and the
cumula�ve impact of development on the local environment must be
considered.
Access & Connec�vity
• The programme for delivery of improvements to the Coast Road must be
coordinated and aligned with delivery of major development, including South
Harbour, to ensure sufficient capacity within the road network.
• In par�cular, the poten�al for construc�on and opera�onal traffic from either
South Harbour or ETZ Development rou�ng through Torry (Victoria Road) was
raised as a significant concern.
• Recogni�on that local access and connec�vity to the Green Network in South
Aberdeen, including Tullos Wood and Balnagask-Cove Coast, could be improved.
Decision Making and Local Influence
• Some in the community felt that local voices have not been heard or listened to
in previous decision making around other developments, including Ness Energy-
from-Waste and Aberdeen South Harbour.
• There was some mistrust within the community of local ins�tu�ons and
organisa�ons, in par�cular around how local benefits and commi�ed ac�ons
and mi�ga�ons have been delivered from development.

St Fittick's Park
East Tullos Burn
Tullos Hill Cairns
SSSIs and local nature reserves

Land Ownership
Within the masterplan area there are a range of land ownerships and development
interests. All land within St Fi�ck’s Park and on the coastal strip encompassing land
at Girdleness, Gregness, Doonies is in the ownership of Aberdeen City Council.
The Port of Aberdeen have current lease and ownership interests on land
associated with their development of the South Harbour around Nigg Bay,
including land within St Fi�ck’s Park and at Gregness which are currently being
used for construc�on compounds and storage.
Within the industrial estates of Altens and East Tullos there is a mix of private
ownership interests, reflec�ng their commercial nature and development pa�ern.
The City Council own the ground lease to a number of sites within East Tullos on
Greenwell Road and Greenbank Crescent.
ETZ Ltd has acquired three brownfield sites within the Masterplan area, all on
Hareness Road. These are the Former Richard Irvin House, Former Muller Dairy
Site, and the 6-acre brownfield site of former Trafalgar House. These will be
refurbished, extended, and developed to the highest feasible energy performance
standards, and will provide key hubs of ac�vity within the Masterplan to be
operated by ETZ Ltd and partners (specific detail is provided within Sec�on 4

Planning permissions search for

Aberdeen South Harbour
Craiginches rail sidings
Ness Energy-from-Waste Facility (incineration)
211700/DPP (District Heating Network from Ness)

"Innovation Campus"

An Energy Incubator and Skills Hub will anchor the Innova�on Campus to foster
supply chain community building, technology research and development,
commercialisa�on and manufacturing, alongside targeted business support to
drive entrepreneurship, innova�on and growth. It will include commercial and
industrial manufacturing units and space, purpose designed for innova�ve start-
up and growing SME businesses in the energy transi�on supply chain

"nature-based solutions and in
providing blue-green infrastructure to support place-making."

"enabling crea�on of an accessible development site within the
St Fi�ck’s Park Opportunity Site. "

greenfield Opportunity Sites OP56,
OP62, and OP61,

site-specific measures to integrate
biodiversity into development through landscape frameworks

connec�ng green spaces at
Tullos Wood, Kincorth Hill, St Fi�ck’s, Walker Park, Balnagask Coast, including the
Na�onal Cycle route 1 (NCR1)
 planned
Port of Aberdeen works to footway on Greyhope Road
poten�al
impacts that may arise from economic development within the ETZ, par�cularly
at St Fi�ck’s Park, Gregness, and Doonies


Balnagask golf course - why isn't it being touched and who owns it

"Local Parklets" plans with ACC - how to learn more about support and planning

"Port integrated ac�vity con�guous with Aberdeen South Harbour and with
direct access to quayside.
High-value energy transi�on ac�vity, such as manufacturing, with func�onal
associa�on to Aberdeen South Harbour which precludes it being located
elsewhere. " Apart from the enormous golf course

bp Aberdeen Hydrogen
Energy Ltd. “Hydrogen Hub”.

Local Development Plan for the Opportunity Sites

ERM Dolphyn is in advanced
discussions to make landfall of their offshore green hydrogen produc�on project
at a site within the Hydrogen Campus, providing...

 ETZ Ltd’s. co-investment with the
Offshore Renewable Energy (ORE) Catapult to create a world leading Na�onal
Floa�ng Wind Innova�on Centre (FLOWIC). 

Innovation Campus related planning permission:

[200~Detailed planning permission (210429/DPP) was granted in July 2021 for
development at the western por�on of the site (approximately one third),
adjacent to Ian Wood House. Approved development was for: “erec�on of
mul�-let / start-up units in Class 5 and 6 with ancillary office and associated
parking, infrastructure and landscaping”

https://publicaccess.aberdeencity.gov.uk/online-applications/applicationDetails.do?activeTab=details&keyVal=QQL34OBZI1K00

• Planning permission in principle (210138/PPP) was granted in May 2021 for
development at the eastern por�on of the site. Approved development was for:
“Commercial development, Class 5 and 6 use (circa 5,000 sqm floorspace), with
associated infrastructure and landscaping”
https://publicaccess.aberdeencity.gov.uk/online-applications/applicationDetails.do?activeTab=details&keyVal=QO06OGBZGIC00


